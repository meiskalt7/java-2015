package labs;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Random;
import java.util.Vector;

import static labs.Constants.*;

/**
 * @author Eiskalt on 11.09.2015.
 */
/*������ � �������. ������ 2 �����: ������� � ����. ������� ��������� ������ N1 ������
**� ������������ P1. ����� ��������� ������ N2 ������ � ������������ P2.
*/
//���������� ����� ��������� ArrayList
//�� ����� ���������� �� �������, ����� ������ ��������� ������ "���������", � ����� ��������������� ��������� ��
//��� ���������, �� ����� ������������, ������������, ����������,
// ���������� � �������� �������(����� �� ��� ������ LinkedList) � �������������(Vector/synchronized)

class Habitat extends JFrame {

	static final Object monitor = new Object();
	static final int period[] = {5, 7};
	static final float chance[] = {0.9f, 1.0f};
	static final int priority[] = {5, 5};

	private int countWorkers = 0;
	private int countWarriors = 0;
	private static int time = 0;

	private Vector<Worker> workers = new Vector<>();
	private Vector<Warrior> warriors = new Vector<>();

	private final Random rand = new Random();

	private Font antFont = new Font("Times New Roman", Font.BOLD, 10);
	private Rectangle bounds[] = new Rectangle[]{
			new Rectangle(100, 100, windowWidth, panelHeight + menuHeight),
			new Rectangle(0, 0, 100, 20),
			new Rectangle(0, 20, 100, 20),
			new Rectangle(0, 40, 150, 20)
	};

	private int clientNumber = 2;
	private Graphics2D graphics2D;
	private JPanel drawingPanel;
	private JPanel optionPanel;
	private JButton startButton;
	private JButton stopButton;
	private JButton saveButton;
	private JButton loadButton;
	private JButton exitButton;
	private JButton connectButton;
	private JButton receiveButton;
	private JToggleButton showInfoButton;
	private JToggleButton workersThreadButton;
	private JToggleButton warriorsThreadButton;
	private JRadioButton showTimeButton;
	private JRadioButton hideTimeButton;
	static JComboBox cbClients = new JComboBox();
	private JLabel countWorkersLabel = new JLabel();
	private JLabel countWarriorsLabel = new JLabel();
	private JLabel simTime = new JLabel();
	private ButtonGroup groupShowHide;
	private Client client;

	private Timer timer;
	private Timer repaintTime;
	private WorkerBaseAI workAI;
	private WarriorBaseAI warAI;

	public Habitat() {
		super("Lab4SIT");
		setupGUI();
	}

	public void startTimer() {
		timer = new Timer(100, e -> {
			time++;
			simTime.setText("SIMULATION TIME: " + time);
			synchronized (monitor) {
				update(time);
				monitor.notifyAll();
			}
		});
		timer.start();
		startMoving();
		repaintTime = new Timer(1, e -> repaint());
		repaintTime.start();
	}

	private void startMoving() {
		workAI = new WorkerBaseAI(getWorkers());
		workAI.thread.setPriority(priority[0]);
		warAI = new WarriorBaseAI(getWarriors());
		warAI.thread.setPriority(priority[1]);
	}

	private void connect() {
		client = new Client(workers, warriors, clientNumber);
		client.connect();
		this.setTitle("client�" + clientNumber);
	}

	private void receive() {
		if (cbClients.getSelectedItem() != null) {
			client.getAnts((int) cbClients.getSelectedItem());
			workers.addAll(client.getWorkers());
			warriors.addAll(client.getWarriors());
		}
	}

	private void motionWarriorAI(boolean suspended) {
		if (!suspended) {
			warAI.resume();
		} else {
			warAI.suspend();
		}
	}

	private void motionWorkersAI(boolean suspended) {
		if (!suspended) {
			workAI.resume();
		} else {
			workAI.suspend();
		}
	}

	private void setupGUI() {
		setBounds(bounds[0]);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setVisible(true);
		setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));
		setLocationRelativeTo(null);
		setResizable(false);
		setFocusable(true);

		initComponents();

		addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent event) {
				processKey(event);
			}
		});
	}

	private void initComponents() {
		initPanels();
		initButtons();
		initLabels();
	}

	private void initPanels() {
		drawingPanel = new JPanel() {
			@Override
			public void paintComponent(Graphics graphics) {
				super.paintComponents(graphics);
				graphics2D = (Graphics2D) graphics.create();
				graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
				graphics2D.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);

				graphics2D.setColor(Color.white);

				BufferedImage grass = null;
				try {
					grass = ImageIO.read(new File("grass.png"));
				} catch (IOException ex) {
				}
				TexturePaint gp = new TexturePaint(grass, new Rectangle(0, 0, 32, 32));
				graphics2D.setPaint(gp);
				graphics2D.fillRect(0, 0, getWidth(), getHeight());

				paintAnts(graphics2D);
			}
		};

		drawingPanel.setAlignmentY(Component.TOP_ALIGNMENT);
		drawingPanel.setMinimumSize(new Dimension(windowWidth, 480));
		drawingPanel.setPreferredSize(new Dimension(windowWidth, 480));
		add(drawingPanel);

		optionPanel = new JPanel();
		optionPanel.setAlignmentY(Component.BOTTOM_ALIGNMENT);
		optionPanel.setLayout(new GridLayout(6, 2));
		add(optionPanel);
	}

	private void initButtons() {
		startButton = new JButton("�����");
		stopButton = new JButton("����");
		showInfoButton = new JToggleButton("���������� ����������");
		showTimeButton = new JRadioButton("���������� ����� ���������");
		hideTimeButton = new JRadioButton("�������� ����� ���������");
		workersThreadButton = new JToggleButton("��������");
		warriorsThreadButton = new JToggleButton("��������");
		saveButton = new JButton("���������");
		loadButton = new JButton("���������");
		exitButton = new JButton("�����");
		connectButton = new JButton("Connect");
		receiveButton = new JButton("��������");
		cbClients = new JComboBox();

		groupShowHide = new ButtonGroup();
		groupShowHide.add(showTimeButton);
		groupShowHide.add(hideTimeButton);

		startButton.addActionListener(evt -> startSimulation());
		stopButton.addActionListener(evt -> stopSimulation());
		showInfoButton.addActionListener(evt -> displayStatistic(showInfoButton.isSelected()));
		showTimeButton.addActionListener(evt -> displayTime(true));
		hideTimeButton.addActionListener(evt -> displayTime(false));
		workersThreadButton.addActionListener(evt -> motionWorkersAI(workersThreadButton.isSelected()));
		warriorsThreadButton.addActionListener(evt -> motionWarriorAI(warriorsThreadButton.isSelected()));
		saveButton.addActionListener(evt -> saveAnts());
		loadButton.addActionListener(evt -> loadAnts());
		exitButton.addActionListener(evt -> exit());
		connectButton.addActionListener(evt -> connect());
		receiveButton.addActionListener(evt -> receive());
		cbClients.addPopupMenuListener(new PopupMenuListener() {
			@Override
			public void popupMenuWillBecomeVisible(PopupMenuEvent event) {
				if (client.isConnected()) {
					client.updateClientList();
				}
			}

			@Override
			public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {

			}

			@Override
			public void popupMenuCanceled(PopupMenuEvent e) {

			}
		});

		startButton.setEnabled(false);

		hideTimeButton.setSelected(true);

		startButton.setFocusable(false);
		stopButton.setFocusable(false);
		showInfoButton.setFocusable(false);
		showTimeButton.setFocusable(false);
		hideTimeButton.setFocusable(false);
		workersThreadButton.setFocusable(false);
		warriorsThreadButton.setFocusable(false);
		saveButton.setFocusable(false);
		loadButton.setFocusable(false);
		connectButton.setFocusable(false);
		cbClients.setFocusable(false);
		receiveButton.setFocusable(false);

		optionPanel.add(startButton);
		optionPanel.add(stopButton);
		optionPanel.add(showInfoButton);
		optionPanel.add(showTimeButton);
		optionPanel.add(hideTimeButton);
		optionPanel.add(workersThreadButton);
		optionPanel.add(warriorsThreadButton);
		optionPanel.add(saveButton);
		optionPanel.add(loadButton);
		optionPanel.add(exitButton);
		optionPanel.add(connectButton);
		optionPanel.add(cbClients);
		optionPanel.add(receiveButton);
	}

	private void initLabels() {
		setLabel(countWorkersLabel, "WORKERS: " + countWorkers, bounds[1]);
		setLabel(countWarriorsLabel, "WARRIORS: " + countWarriors, bounds[2]);
		setLabel(simTime, "SIMULATION TIME: " + time, bounds[3]);
	}

	private void processKey(KeyEvent ev) {
		switch (ev.getKeyCode()) {
			case KeyEvent.VK_B:
				if (startButton.isEnabled()) {
					startSimulation();
				}
				break;
			case KeyEvent.VK_E:
				if (!startButton.isEnabled()) {
					stopSimulation();
				}
				break;
			case KeyEvent.VK_T:
				displayTime(!simTime.isVisible());
				break;
		}
	}

	private void update(int time) {
		if (time % period[0] == 0 && rand.nextFloat() < chance[0] && workers.size() <= 2) {
			workers.add(new Worker());
			countWorkers++;
			countWorkersLabel.setText("WORKERS: " + countWorkers);
		}
		if (time % period[1] == 0 && rand.nextFloat() < chance[1] && warriors.size() <= 2) {
			warriors.add(new Warrior());
			countWarriors++;
			countWarriorsLabel.setText("WARRIORS: " + countWarriors);
		}
	}

	private void paintAnts(Graphics2D graphics2D) {
		for (AbstractAnt ant : workers) {
			ant.paint(graphics2D);
		}

		for (AbstractAnt ant : warriors) {
			ant.paint(graphics2D);
		}

	}

	private void startSimulation() {
		reset();
		resumeSimulation();
	}

	private void reset() {
		workers.clear();
		warriors.clear();
		countWorkers = 0;
		countWarriors = 0;
		time = 0;
	}

	private void resumeSimulation() {
		repaint();
		timer.start();
		repaintTime.start();

		workAI.resume();
		warAI.resume();

		countWorkersLabel.setText("WORKERS: " + countWorkers);
		countWarriorsLabel.setText("WARRIORS: " + countWarriors);
		startButton.setEnabled(false);
		stopButton.setEnabled(true);
	}

	private void stopSimulation() {
		displayStatistic(true);

		timer.stop();
		repaintTime.stop();

		workAI.suspend();
		warAI.suspend();

		startButton.setEnabled(true);
		stopButton.setEnabled(false);

		if (showInfoButton.isSelected()) {
			showInfoButton.setSelected(false);
			showDialog();
		}
	}

	private void displayStatistic(boolean visibility) {
		countWorkersLabel.setVisible(visibility);
		countWarriorsLabel.setVisible(visibility);
	}

	private void displayTime(boolean visibility) {
		if (simTime.isVisible()) {
			hideTimeButton.setSelected(true);
		} else {
			showTimeButton.setSelected(true);
		}
		simTime.setVisible(visibility);
	}

	private void setLabel(JLabel label, String text, Rectangle bounds) {
		label.setText(text);
		label.setBounds(bounds);
		label.setFont(antFont);
		label.setForeground(Color.BLUE);
		label.setVisible(false);
		optionPanel.add(label);
	}

	private void showDialog() {
		Object[] options = {"��", "������"};

		TextArea infoMessage = new TextArea("WORKERS: " + countWorkers + "\nWARRIORS: " + countWarriors + "\nSIMULATION TIME: " + time);
		infoMessage.setEditable(false);

		int n = JOptionPane.showOptionDialog(this, infoMessage,
				"Info", JOptionPane.CLOSED_OPTION,
				JOptionPane.INFORMATION_MESSAGE, null, options,
				null);
		if (n == 0) {
			stopSimulation();
		} else {
			resumeSimulation();
		}
	}

	private void saveAnts() {
		ObjectOutputStream oos;
		try {
			oos = new ObjectOutputStream(new FileOutputStream("workers.ser"));
			oos.writeObject(workers);
			for (Worker ant : workers) {
				oos.writeObject(ant);
			}
			oos.flush();
			oos.close();

			oos = new ObjectOutputStream(new FileOutputStream("warriors.ser"));
			oos.writeObject(warriors);
			for (Warrior ant : warriors) {
				oos.writeObject(ant);
			}
			oos.flush();
			oos.close();
		} catch (FileNotFoundException e) {
			System.out.println("FileNotFoundException");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("IOException");
			e.printStackTrace();
		}
	}

	private void loadAnts() {
		ObjectInputStream oin;
		try {
			oin = new ObjectInputStream(new FileInputStream("workers.ser"));
			workers.addAll((Vector<Worker>) oin.readObject());
			countWorkers += workers.size() - countWorkers;

			oin = new ObjectInputStream(new FileInputStream("warriors.ser"));
			warriors.addAll((Vector<Warrior>) oin.readObject());
			countWarriors += warriors.size() - countWarriors;
		} catch (FileNotFoundException e) {
			System.out.println("FileNotFoundException");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("IOException");
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			System.out.println("ClassNotFoundException");
			e.printStackTrace();
		}
	}

	private void exit() {
		if (client != null && client.isConnected()) {
			client.removeClient(clientNumber);
		}
		stopSimulation();
		reset();
		workAI = null;
		warAI = null;
		dispose();
	}

	private Vector<Worker> getWorkers() {
		return workers;
	}

	private Vector<Warrior> getWarriors() {
		return warriors;
	}
}
