package labs;

import javax.swing.*;
import java.io.*;

/**
 * @author Eiskalt on 12.09.2015.
 */
public class Toolbar extends JFrame {

    private JPanel toolbarPanel;
    private JTextField tfWorker;
    private JTextField tfWarrior;
    private JComboBox cbWorker;
    private JComboBox cbWarrior;
    private JButton acceptButton;
    private JComboBox cbWorkerPriority;
    private JComboBox cbWarriorPriority;
    private JButton saveButton;
    private JButton loadButton;

    public Toolbar() {
        super("Toolbar");
    }

    public void openToolbar() {
        setContentPane(new Toolbar().toolbarPanel);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        pack();
        setVisible(true);
        setLocationRelativeTo(null);
        setResizable(false);
    }

    private void setParameters() {
        try {
            Habitat.period[0] = Integer.parseInt(tfWorker.getText());
            Habitat.period[1] = Integer.parseInt(tfWarrior.getText());

            Habitat.chance[0] = Float.valueOf(cbWorker.getSelectedItem().toString());
            Habitat.chance[1] = Float.valueOf(cbWarrior.getSelectedItem().toString());

            Habitat.priority[0] = Integer.parseInt(cbWorkerPriority.getSelectedItem().toString());
            Habitat.priority[1] = Integer.parseInt(cbWarriorPriority.getSelectedItem().toString());

            System.out.println("Conversion was successful.");
        } catch (NumberFormatException nfe) {
            JOptionPane.showMessageDialog(this, "Sorry. Incompatible data.", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
        cbWorker = new JComboBox();
        cbWarrior = new JComboBox();
        cbWorkerPriority = new JComboBox();
        cbWarriorPriority = new JComboBox();


        cbWorker.setModel(new DefaultComboBoxModel(new String[]{"0", "10", "20", "30", "40", "50", "60", "70", "80", "90", "100"}));
        cbWarrior.setModel(new DefaultComboBoxModel(new String[]{"0", "10", "20", "30", "40", "50", "60", "70", "80", "90", "100"}));
        cbWorkerPriority.setModel(new DefaultComboBoxModel(new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}));
        cbWarriorPriority.setModel(new DefaultComboBoxModel(new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}));


        cbWorker.setSelectedIndex(9);
        cbWarrior.setSelectedIndex(3);
        cbWorkerPriority.setSelectedIndex(5);
        cbWarriorPriority.setSelectedIndex(5);

        acceptButton = new JButton();
        acceptButton.addActionListener(e -> setParameters());

        saveButton = new JButton();
        saveButton.addActionListener(e -> saveConfig());

        loadButton = new JButton();
        loadButton.addActionListener(e -> loadConfig());
    }

    private void saveConfig() {
        try {
            DataOutputStream dos = new DataOutputStream(
                    new FileOutputStream("config.txt"));
            dos.writeUTF(tfWorker.getText());
            dos.writeUTF(tfWarrior.getText());
            dos.writeUTF(cbWorker.getSelectedItem().toString());
            dos.writeUTF(cbWarrior.getSelectedItem().toString());
            dos.writeUTF(cbWorkerPriority.getSelectedItem().toString());
            dos.writeUTF(cbWarriorPriority.getSelectedItem().toString());
            dos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loadConfig() {
        try(DataInputStream dis = new DataInputStream(
                new FileInputStream("config.txt"))) {
            tfWorker.setText(dis.readUTF());
            tfWarrior.setText(dis.readUTF());
            cbWorker.setSelectedItem(dis.readUTF());
            cbWarrior.setSelectedItem(dis.readUTF());
            cbWorkerPriority.setSelectedItem(dis.readUTF());
            cbWarriorPriority.setSelectedItem(dis.readUTF());
            //repaint();
        } catch (FileNotFoundException e) {
            System.out.println("FileNotFoundException");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("IOException");
            e.printStackTrace();
        }
    }
}
