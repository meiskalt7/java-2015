package labs;

import javax.swing.*;
import java.awt.*;
import java.io.Serializable;
import java.util.Random;

import static labs.Constants.*;

/**
 * @author Eiskalt on 11.09.2015.
 */
abstract class AbstractAnt implements IBehaviour, Serializable {

    private Random rand = new Random();
    ImageIcon image;
    double posX, posY, startX, startY;


    AbstractAnt() {
        startX = rand.nextInt(windowWidth-20);
        startY = rand.nextInt(panelHeight-20);
        posX = startX;
        posY = startY;
    }

    void setImage(String path) {
        image = new ImageIcon(path);
    }

    public abstract void paint(Graphics2D graphics2D);

    public abstract void move();
}
