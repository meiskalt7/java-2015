package labs;

import java.awt.*;

/**
 * @author Eiskalt on 11.09.2015.
 */
public class Warrior extends AbstractAnt {

    private int radius = 2;
    private double angle = 0, speed = 0.9;

    Warrior() {
        super();
        setImage("warriorSprite.png");
    }

    @Override
    public void paint(Graphics2D graphics2D) {
        image.paintIcon(null, graphics2D, (int)posX, (int)posY);
    }


    public synchronized void move() {
        posX += radius * Math.cos(angle);
        posY += radius * Math.sin(angle);
        angle += speed;
    }
}
