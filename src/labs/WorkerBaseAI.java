package labs;

import java.util.Vector;

/**
 * @author Eiskalt on 15.09.2015.
 */
/*
1.	�������-������� ��������� � ���� �� ����� ������� �� �������� (��������, [0;0])
    �� ������ �� ��������� V, � ����� ������������ ������� � ����� ������ ��������
    � ��� �� ���������.
 */
public class WorkerBaseAI extends BaseAI {

    Thread thread;
    private volatile Vector<Worker> workers;
    private boolean suspendFlag;

    WorkerBaseAI(Vector<Worker> workers) {
        thread = new Thread(this, "WorkerBaseAI");
        System.out.println("������ ������ �����");
        thread.start();
        this.workers = workers;
        suspendFlag = false;
    }

    @Override
    public void run() {
        try {
            while (true) {
                synchronized (Habitat.monitor) {
                    while (suspendFlag) {
                        Habitat.monitor.wait();
                    }
                    try {
                        Habitat.monitor.wait();
                        workers.forEach(Worker::move);
                        Thread.sleep(100);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("������");
        } finally {
            System.out.println("������ ����� ��������");
        }
    }

    public void suspend() {
        suspendFlag = true;
    }

    public void resume() {
        synchronized (Habitat.monitor) {
            suspendFlag = false;
            Habitat.monitor.notifyAll();
        }
    }
}
