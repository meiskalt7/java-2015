package labs;

import java.awt.*;

import static labs.Constants.panelHeight;
import static labs.Constants.windowWidth;

/**
 * @author Eiskalt on 11.09.2015.
 */
public class Worker extends AbstractAnt {
	private static final int TO_SPAWN = 0;
	private static final int TO_CORNER = 1;

	private int cornerX;
	private int cornerY;
	private double pathLengthX, pathLengthY; //����
	private double directionX, directionY; //����������� ��������
	private double speed = 0.8;
	private int status = TO_CORNER;

	Worker() {
		super();
		setImage("workerSprite.png");
		setNearestCorner();
	}

	@Override
	public void paint(Graphics2D graphics2D) {
		image.paintIcon(null, graphics2D, (int) posX, (int) posY);
	}

	public synchronized void move() {

		posX += directionX * speed;
		posY += directionY * speed;

		if (status == TO_CORNER
				&& (posX >= windowWidth
				&& posY >= panelHeight || posX >= windowWidth
				&& posY <= 0 || posX <= 0
				&& posY >= panelHeight || posX <= 0
				&& posY <= panelHeight)) {
			setDelta(startX, startY); //������������ �� �����
			status = TO_SPAWN;
		}
		if (status == TO_SPAWN && Math.abs(startX - cornerX) <= Math.abs(posX - cornerX)
				&& Math.abs(startY - cornerY) <= Math.abs(posY - cornerY)) {
			directionX = 0;
			directionY = 0;
		}
	}

	private void setDelta(double endX, double endY) {
		pathLengthX = endX - posX;
		pathLengthY = endY - posY;
		directionX = pathLengthX / 12;
		directionY = pathLengthY / 12;
	}

	private void setNearestCorner() {
		if (posX >= windowWidth / 2 & posY >= panelHeight / 2) {
			cornerX = windowWidth - 20;
			cornerY = panelHeight - 20;
		} else if (posX < windowWidth / 2 & posY < panelHeight / 2) {
			cornerX = 0;
			cornerY = 0;
		} else if (posX < windowWidth / 2 & posY > panelHeight / 2) {
			cornerX = 0;
			cornerY = panelHeight - 20;
		} else {
			cornerX = windowWidth - 20;
			cornerY = 0;
		}
		setDelta(cornerX, cornerY);
	}
}
