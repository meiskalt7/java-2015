package labs;

import java.util.Vector;

/**
 * @author Eiskalt on 17.09.2015.
 */
public class WarriorBaseAI extends BaseAI {
    Thread thread;
    private volatile Vector<Warrior> warriors;
    private boolean suspendFlag;

    WarriorBaseAI(Vector<Warrior> warriors) {
        thread = new Thread(this, "WarriorBaseAI");
        System.out.println("������ ������ �����");
        thread.start();
        this.warriors = warriors;
        suspendFlag = false;
    }

    @Override
    public void run() {
        try {
            while (true) {
                synchronized (Habitat.monitor) {
                    while (suspendFlag) {
                        Habitat.monitor.wait();
                    }
                    try {
                        Habitat.monitor.wait();
                        roundMove();
                        Thread.sleep(10);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            System.out.println("������ ����� ��������");
        }
    }

    private void roundMove() {
        warriors.forEach(Warrior::move);
    }

    public void suspend() {
        suspendFlag = true;
    }

    public void resume() {
        synchronized (Habitat.monitor) {
            suspendFlag = false;
            Habitat.monitor.notifyAll();
        }
    }
}
