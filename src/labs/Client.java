package labs;

import java.io.DataOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Random;
import java.util.Vector;

/**
 * @author Eiskalt on 17.09.2015.
 */


class Client {

    private Vector<Worker> workers;
    private Vector<Warrior> warriors;
    private String host = "localhost";
    private int port = 3333;
    private int clientNumber;
    private Socket sock;
    private DataOutputStream outputStream = null;
    private ObjectOutputStream oos = null;
    private ObjectInputStream ois = null;
    private static ArrayList<Integer> clientList = new ArrayList<>();
    private boolean connected = false;

    Client(Vector<Worker> workers, Vector<Warrior> warriors, int clientNumber) {
        try {
            sock = new Socket(host, port);
            outputStream = new DataOutputStream(sock.getOutputStream());
            ois = new ObjectInputStream(sock.getInputStream());
            this.clientNumber = clientNumber;
        } catch (ConnectException e) {
            System.out.println("Server not found");
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.workers = workers;
        this.warriors = warriors;
    }

    //public void run() {
    //}

    public void connect() {
        try {
            System.out.println("Connecting...");

            outputStream.writeInt(1);

            outputStream.writeInt(clientNumber);

            oos = new ObjectOutputStream(sock.getOutputStream());
            System.out.println("oos+");
            oos.writeObject(workers);
            oos.writeObject(warriors);

            Habitat.cbClients.removeAllItems();
            clientList = (ArrayList) ois.readObject();
            for (int client : clientList) {
                Habitat.cbClients.addItem(client);
            }
            connected = true;
            System.out.println("Connected");
        } catch (NullPointerException e) {
            System.out.println("Connection refused");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getAnts(int senderNumber) {
        try {
            System.out.println("Getting Ants");
            sock = new Socket(host, port);
            outputStream = new DataOutputStream(sock.getOutputStream());
            ois = new ObjectInputStream(sock.getInputStream());
            outputStream.writeInt(2); //operation
            outputStream.writeInt(senderNumber); //client from
            Random random = new Random();
            outputStream.writeInt(random.nextInt(3));

            System.out.println("Getting list");
            workers = (Vector<Worker>) ois.readObject();
            warriors = (Vector<Warrior>) ois.readObject();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateClientList() {
        try {
            System.out.println("Updating...");
            sock = new Socket(host, port);
            outputStream = new DataOutputStream(sock.getOutputStream());
            ois = new ObjectInputStream(sock.getInputStream());

            outputStream.writeInt(0);
            outputStream.writeInt(clientNumber);

            System.out.println("cbClients...");

            Habitat.cbClients.removeAllItems();
            clientList = (ArrayList) ois.readObject();
            for (int client : clientList) {
                Habitat.cbClients.addItem(client);
            }
            System.out.println("clientList updated");
        } catch (ConnectException e) {
            System.out.println("Connection refused");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void removeClient(int clientNumber) {
        try {
            System.out.println("Deleting...");
            sock = new Socket(host, port);
            outputStream = new DataOutputStream(sock.getOutputStream());

            outputStream.writeInt(3);

            outputStream.writeInt(clientNumber);
        } catch (NullPointerException | ConnectException e) {
            System.out.println("Not connected...");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Vector<Worker> getWorkers() {
        return workers;
    }

    public Vector<Warrior> getWarriors() {
        return warriors;
    }

    public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }
}
