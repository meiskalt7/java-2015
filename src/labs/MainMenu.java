package labs;

import javax.swing.*;

/**
 * @author Eiskalt on 12.09.2015.
 */
public class MainMenu {
    private JButton bSimulation;
    private JButton bToolbar;
    private JButton bExit;
    private JPanel menuPanel;
    private Habitat app;
    private Toolbar toolbarWindow;

    public static void main(String[] args) {
        JFrame frame = new JFrame("MainMenu");
        frame.setContentPane(new MainMenu().menuPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
    }

    public MainMenu() {
        initComponents();
    }

    private void initComponents() {
        bSimulation.addActionListener(evt -> openSimulationWindow());
        bToolbar.addActionListener(evt -> openToolbarWindow());
        bExit.addActionListener(evt -> System.exit(0));
    }

    private void openSimulationWindow() {
        app = new Habitat();
        app.startTimer();
    }

    private void openToolbarWindow() {
        toolbarWindow = new Toolbar();
        toolbarWindow.openToolbar();
    }
}
