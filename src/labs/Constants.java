package labs;

/**
 * @author Eiskalt on 17.09.2015.
 */
final class Constants {
    public static final int windowWidth = 640, panelHeight = 480, menuHeight = 200;
    private Constants() {}
}
